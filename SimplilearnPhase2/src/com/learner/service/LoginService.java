package com.learner.service;

import com.learner.model.Login;

public interface LoginService {

	public String validateLogin(Login login);
}
