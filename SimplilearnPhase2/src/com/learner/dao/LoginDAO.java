package com.learner.dao;

import com.learner.model.Login;

public interface LoginDAO {

	public String validateLogin(Login login);
}
